#!/usr/bin/env python
"""Tasks for loading and updating the Aegir configuration files"""

import os
import yaml

from jenkins import Jenkins
from fabric.api import  abort, env

def get(key):
    """Get part of the Aegir configuration as set in the "config" task.

    Fabric configuration is stored in a global dictionary called "env". Custom
    configuration can be attached to this dictionary.
    """

    try:
        return env.aegir[key]
    except KeyError:
        abort("Aegir config key '%s' not recognized or set." % key)

def load():
    """Import the aegir configuration.

    Configuration resides in several YAML files, which by default are found at
    config/ in the current working directory.
    """

    directory = os.path.realpath(os.path.join(os.getcwd(), 'config'))

    configs = [
        'local',
        'host',
        'lock',
        'platforms',
        'sites',
        'build_servers',
        'keys'
    ]

    env.aegir = {}

    for config in configs:
        path = os.path.join(directory, config + '.yml')

        if not os.path.isfile(path):
            abort("Configuration file '%s' not found." % path)

        with open(path, 'r') as stream:
            env.aegir[config] = yaml.load(stream) or {}

        stream.close()

    for name, config in env.aegir['build_servers'].iteritems():
        instance = Jenkins(config['uri'],
                           username=config['username'],
                           password=env.aegir['keys'][name])
        env.aegir['build_servers'][name]['instance'] = instance

    env.warn_only = True
    env.hosts = [env.aegir['host']['uri']]
    env.forward_agent = bool(env.aegir['local']['forward_agent'])
    env.user = env.aegir['local']['user']
    env.sudo_user = env.aegir['host']['aegir_user']
