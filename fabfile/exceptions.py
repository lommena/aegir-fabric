#!/usr/bin/env python
"""Defined exceptions for Aegir library"""

class UndefinedAliasError(Exception):
    """Exception thrown when an attempt to get an alias record fails.

    This may occur if the context does not exist, or the alias was mis-typed.
    """

    pass
