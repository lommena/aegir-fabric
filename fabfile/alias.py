#!/usr/bin/env python
"""Helper functions for working with drush aliases"""

import re
import yaml
import fabfile.drush as drush

from fabric.api import hide
from fabric.utils import warn
from fabfile.exceptions import UndefinedAliasError

def exists(context):
    """Check if the given alias is defined on the remote server.

    Returns true if the alias is found, false otherwise.
    """

    return context in get_all_aliases()

def convert_keys(alias_records):
    """Add ampersands to all keys of the given alias records.

    By default, drush outputs alias records keyed by the alias without the
    preceeding ampersand. However, aliases contained in alias records (e.g. a
    database server alias) do have the ampersand.

    For consistency and ease of programming, it is useful to add the ampersand
    to the top-level keys of alias record dictionaries.
    """

    if not isinstance(alias_records, dict):
        raise TypeError('Alias record must be of type dict')

    return {'@%s' % key: alias_records[key] for key in alias_records}

def get_all_aliases():
    """Load all aliases without associated record data.

    Conserves bandwidth when communicating with the remote server.
    """

    aliases = []

    with hide('everything'):
        output = drush.run('site-alias')

    if not output.succeeded:
        warn('Alias list failed to load')
    else:
        for line in output.splitlines():
            aliases.append('@%s' % line)

    return aliases

def get_record(context):
    """Load a drush alias into a Python dictionary.

    The alias argument must be a fully-formed, valid alias, including the
    prceeding ampersand.
    """

    with hide('everything'):
        output = drush.run('site-alias', context, '--format=yaml')

    if not output.succeeded:
        raise UndefinedAliasError("Failed to load alias %s" % context)

    alias_record = yaml.load(output)

    if len(alias_record) == 1:
        alias_record = alias_record.itervalues().next()
    else:
        alias_record = convert_keys(alias_record)

    return alias_record

def get_all_records(context_type=None):
    """Load all drush aliases into a Python dictionary."""

    with hide('everything'):
        output = drush.run('site-alias', '--format=yaml')

    alias_records = yaml.load(output)

    if context_type:
        alias_records = {key: value for key, value in alias_records.iteritems()
                         if value['context_type'] == context_type}

    alias_records = convert_keys(alias_records)

    return alias_records

def machine_name(string):
    """Get the macine name of the given context.

    By convention, machine names contain only letters, numbers, and underscores.
    All other symbols are stripped out. Consecutive underscores are collapsed
    into a single occurence.
    """

    converted_string = re.sub('[^a-zA-Z0-9_]', '', string)
    converted_string = re.sub('_+', '_', converted_string)

    return converted_string

def platform(platform_name):
    """Get the platform alias for the given token.

    By convention, platform aliases are the machine name of the platform
    prepended by "@platform_".
    """

    return '@platform_%s' % machine_name(platform_name)

def server(server_name):
    """Get the server alias for the given token.

    By convention, server aliases are the machine name of the server prepended
    by "@server_".
    """
    return '@server_%s' % machine_name(server_name)

def site(site_name):
    """Get the site alias for the given token.

    Since site aliases are formed from the URI of a site, this simply invovles
    prepending the given token with an ampersand.
    """

    return '@%s' % site_name
