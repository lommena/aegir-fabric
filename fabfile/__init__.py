#!/usr/bin/env python
"""Aegir Fabric"""

import fabfile.provision as provision
import fabfile.hosting as hosting
import fabfile.platform as platform
import fabfile.site as site
import fabfile.build_server as build_server
import fabfile.config as config

config.load()
