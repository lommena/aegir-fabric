#!/usr/bin/env python
"""Defined tasks that pertain to site contexts"""

import sys

import fabfile.config as config
import fabfile.alias as alias

from fabric.api import abort, cd, env, execute, put, settings, sudo, task
from fabric.utils import error
from fabfile.exceptions import UndefinedAliasError

@task
def deploy(site_name, backup, old_uri):
    """Deploy a site.

    Restore the given complete Aegir site backup, then verify and enable the
    site on the frontend.
    """

    host = config.get('host')
    site = config.get('sites').get(site_name)

    if not site:
        abort("Site %s is not defined." % site_name)

    site_alias = alias.site(site_name)
    platform_name = site['platform']
    platform_alias = alias.platform(platform_name)

    try:
        platform = alias.get_record(platform_alias)
    except UndefinedAliasError:
        abort("Platform %s is not defined." % site['platform'])

    temp_dir = host.get('temp_dir', '/tmp')
    upload = temp_dir + '/' + backup
    path_components = (host['aegir_root'], platform_name, site_name)
    path = '%s/platforms/%s/sites/%s' % path_components
    server = alias.get_record(platform['web_server'])

    put(backup, temp_dir)
    execute('provision.deploy_site', site_alias, upload, old_uri)

    with cd(path):
        sudo('rm -f local.settings.php')

    with settings(host_string=server['remote_host']), cd(path):
        sudo("find files/ -type d -exec chmod 2770 '{}' \\;")
        sudo("find files/ -type f -exec chmod 0660 '{}' \\;")
        sudo("find private/ -type d -exec chmod 2770 '{}' \\;")
        sudo("find private/ -type f -exec chmod 0660 '{}' \\;")

    execute('hosting.verify_platform', platform_alias)
    execute('hosting.enable_site', site_alias)

@task
def install(site_name):
    """Install the given site on the remote.

    This task will read the profile to use and run the provision install task on
    the Aegir server. This assumes that the site has already been saved; thus
    drush will read the platform and other configuration from the existing drush
    alias for the site.
    """

    site = config.get('sites').get(site_name)

    if not site:
        abort("Site %s is not defined." % site_name)

    execute('provision.install_site', site_name, site['profile'])

@task
def revert(site_alias):
    """Restore the backup of a production site during an upgrade.

    This task clones the backup site at "backup.<URI>" to <URI>, the production
    URI of the site.

    Note that this task is independant of the built-in Aegir sites backups.

    This task does not perform cleanup of the old site, in case the restore
    process fails.
    """

    try:
        site = alias.get_record(site_alias)
        backup_site_alias = alias.site('backup.' + site['uri'])
        backup_site = alias.get_record(backup_site_alias)

        result = execute('provision.delete', site_alias)

        if not result[env.host_string].succeeded:
            abort("Could not revert %s, deletion failed" % site_alias)

        result = execute('provision.clone_site', backup_site_alias, site_alias,
                backup_site['platform'])

        if not result[env.host_string].succeeded:
            abort("Could not revert %s, clone of %s failed" %
                  (site_alias, backup_site_alias))

        execute('provision.verify', site_alias)

        result = execute('hosting.import_context', site_alias)
    except UndefinedAliasError as detail:
        error(str(detail))

@task
def revert_all(platform_alias):
    """Revert all sites migrated during an upgrade process.

    All sites on the given platform will be reverted, unless they are in the
    lock configuration file to prevent changes.
    """

    sites = alias.get_all_records(context_type='site')

    for site_alias, site in sites.iteritems():
        if platform_alias != site['platform']:
            continue

        if site_alias in config.get('lock'):
            continue

        execute('build_server.build_job', 'jenkins', 'revert_site',
                SITE_ALIAS=site_alias)

@task
def save(site_name):
    """Load the configuration for a site and provision-save it.

    Note that this task will not create a node on the frontend. Run "hosting_
    import_site" or "deploy" to build a new site from this task.
    """

    site = config.get('sites').get(site_name)

    if not site:
        abort("Site %s is not defined." % site_name)

    execute('provision.save_site', site_name, **{
        'client_name': 'admin',
        'db_server': site['db_server'],
        'platform': site['platform'],
        'profile': site['profile'],
        'uri': site['uri']
    })

@task
def verify_all():
    """Verify all sites."""

    sites = alias.get_all_records(context_type='site')

    for site_alias in sites:
        execute('provision.verify', site_alias)

@task
def clone_all(source_platform_alias, target_platform_alias):
    """Clone all sites to the new platform specified in upgrade.yml.

    This leaves the production site running while the cloned site is available
    for testing. In the event that an error is detected, the cloned site can
    simply be deleted without affecting the production instance.

    This task iterates over the sites defined in current-sites.yml and clones
    each one to the new URI "<subdomain>clone.wwu.edu". This cloned instance is
    not imported to the Aegir frontend and is not publicly visible.
    """

    sites = alias.get_all_records(context_type='site')

    for site_alias, site in sites.iteritems():
        if source_platform_alias != site['platform']:
            continue

        if site_alias in config.get('lock'):
            continue

        if 'backup' in site_alias:
            continue

        if 'clone' in site_alias:
            continue

        new_site_alias = alias.site('clone.' + site['uri'])

        parameters = {
            'SITE_ALIAS': site_alias,
            'NEW_SITE_ALIAS': new_site_alias,
            'TARGET_PLATFORM_ALIAS': target_platform_alias
        }

        execute('build_server.build_job', 'jenkins', 'clone_site', **parameters)

@task
def delete_backups():
    """Delete all backup sites generated from the "migrate sites" task.

    Intended for use after the sites have been stable for awhile, or the backups
    need to be cleared for some reason.
    """

    sites = alias.get_all_records(context_type='site')

    for site in sites.itervalues():
        site_alias = alias.site(site['uri'])

        if site_alias in config.get('lock'):
            continue

        if 'backup' in site_alias:
            continue

        if 'clone' in site_alias:
            continue

        backup_site_alias = alias.site('backup.' + site['uri'])

        if not alias.exists(backup_site_alias):
            continue

        execute('provision.delete', backup_site_alias)

@task
def delete_clones():
    """Delete all sites generated from the "clone sites" task.

    In the case of a significant failure of the clone part of the upgrade
    process, use this job to attempt to remove the cloned sites and start fresh.
    """

    sites = alias.get_all_records(context_type='site')

    for site in sites.itervalues():
        site_alias = alias.site(site['uri'])

        if site_alias in config.get('lock'):
            continue

        if 'backup' in site_alias:
            continue

        if 'clone' in site_alias:
            continue

        clone_site_alias = alias.site('clone.' + site['uri'])

        if not alias.exists(clone_site_alias):
            continue

        execute('provision.delete', clone_site_alias)

@task
def migrate_all(source_platform_alias, target_platform_alias):
    """Migrate sites cloned to a temporary clone URI to the production URI.

    This task moves the cloned test instance of a production site (created by
    the "clone_sites" task) to the new production platform. It iterates over
    each entry in the current-sites.yml configuration file and migrates
    (renames) them to "<subdomain>backup.wwu.edu"

    The cloned test site at "<subdomain>clone.wwu.edu" is then migrated
    (renamed) to <URI> and becomes the publicly visible, live production
    instance of that site.

    The presence of the backup site at "backup.<URI>" serves as an additional
    fallback, separate from the built-in Aegir backup system. This backup
    instance can be quickly restored to production if errors are found by using
    the "revert_site" task.
    """

    sites = alias.get_all_records(context_type='site')

    for site_alias, site in sites.iteritems():
        if source_platform_alias != site['platform']:
            continue

        if site_alias in config.get('lock'):
            continue

        if 'backup' in site_alias:
            continue

        if 'clone' in site_alias:
            continue

        execute('build_server.build_job', 'jenkins', 'safe_migrate_site',
                SITE_ALIAS=site_alias,
                TARGET_PLATFORM_ALIAS=target_platform_alias)

@task
def safe_migrate(site_alias, target_platform_alias):
    """Safely migrate a site using a clone and backup.

    Instead of performing a straight migrate, this task looks for an existing
    clone of the site on the target platform. If it exists, it migrates the old
    site to backup.<url> and migrates the clone site to <url>.
    """

    site = alias.get_record(site_alias)

    backup_site_alias = alias.site('backup.' + site['uri'])
    clone_site_alias = alias.site('clone.' + site['uri'])

    if not alias.exists(clone_site_alias):
        sys.exit(0)

    result = execute('provision.migrate_site', site_alias, site['platform'],
                     backup_site_alias)

    if not result[env.host_string].succeeded:
        abort("Failed to create a backup clone of %s" % site_alias)

    result = execute('provision.migrate_site', clone_site_alias,
                     target_platform_alias, site_alias)

    if not result[env.host_string].succeeded:
        execute('provision.migrate_site', backup_site_alias, site['platform'],
                site_alias)
        abort("Failed to import %s" % site_alias)

    execute('hosting.import_context', site_alias)
