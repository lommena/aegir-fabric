#!/usr/bin/env python
"""Functions for interfacing with Drush"""

from fabric.api import hide, local, sudo

def run(*args, **kwargs):
    """Call drush as the sudo user on the remote host with the given arguments.

    Executes drush on the remote host with the given arguments. Arguments must
    be correct and in the appropriate order for any command that is called. For
    saftey and to avoid blocking, assume "no" as the answer to all prompts.
    """

    drush_args = ['drush']

    if 'debug' in kwargs and kwargs['debug']:
        drush_args.append('--debug')

    drush_args += args

    command = ' '.join(str(arg) for arg in drush_args).strip()

    with hide('warnings'):
        if 'local' in kwargs and kwargs['local']:
            output = local(command)
        else:
            output = sudo(command)

    return output
