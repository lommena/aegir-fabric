#!/usr/bin/env python
"""Tasks for the Hosting component of Aegir"""

import time
import fabfile.drush as drush

from fabric.api import execute, task

@task
def tasks(timeout=30):
    """Force Aegir to process the task queue immediately.

    Wait for 30 seconds by default, to give any tasks time to complete.
    """

    drush.run('@hostmaster', 'hosting-dispatch', '--lock-wait=%d' % 60)
    drush.run('@hostmaster', 'hosting-tasks', '--lock-wait=%d' % 60)
    time.sleep(timeout)

@task
def disable_site(site_alias, force=True):
    """Disable a site on the Aegir frontend."""

    opts = ['@hostmaster', 'hosting-task', site_alias, 'disable']

    if force is True:
        opts.append('--force')

    output = drush.run(*opts)

    execute('hosting.tasks')

    return output

@task
def enable_site(site_alias, force=True):
    """Enable a site on the Aegir frontend."""

    opts = ['@hostmaster', 'hosting-task', site_alias, 'enable']

    if force is True:
        opts.append('--force')

    output = drush.run(*opts)

    execute('hosting.tasks')

    return output

@task
def import_context(context_alias):
    """Import the given context into the frontend."""

    drush.run('@hostmaster', 'hosting-import', context_alias)

@task
def verify_platform(platform_alias, force=True):
    """Verify a platform through the Aegir frontend."""

    opts = ['@hostmaster', 'hosting-task', platform_alias, 'verify']

    if force is True:
        opts.append('--force')

    output = drush.run(*opts)

    for _ in range(4):
        execute('hosting.tasks')

    return output

@task
def verify_site(site_alias, force=True):
    """Verify a site through the Aegir frontend."""

    opts = ['@hostmaster', 'hosting-task', site_alias, 'verify']

    if force is True:
        opts.append('--force')

    output = drush.run(*opts)

    execute('hosting.tasks')

    return output
