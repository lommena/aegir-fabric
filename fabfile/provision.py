#!/usr/bin/env python
"""Tasks for the Provision component of Aegir"""

import fabfile.drush as drush
import fabfile.alias as alias

from fabric.api import task
from fabric.utils import error

def save(context_alias, **kwargs):
    """Save the given context."""

    opts = ['provision-save', context_alias]

    for key, value in kwargs.iteritems():
        opts.append("--%s='%s'" % (key, value))

    output = drush.run(*opts)

    if output.failed:
        error("Save of %s failed:\n%s" % (context_alias, output))

    return output

@task
def clone_site(site_alias, new_site_alias, platform_alias):
    """Clone a site from site alias to new site alias on the given platform.

    Note that this task will not create a new site on the frontend. Additional
    tasks are required to import and enable the site.
    """

    output = drush.run(site_alias, 'provision-clone', new_site_alias,
                       platform_alias)

    if output.failed:
        error("provision-clone of %s failed:\n%s" % (site_alias, output))

    return output

@task
def delete(context_alias):
    """Delete the given context.

    Note that this task will not be registered on the frontend and will leave an
    orphaned context node. This node can be manually deleted by the user 1
    account.
    """

    output = drush.run(context_alias, 'provision-delete')

    if output.failed:
        error("Delete of %s failed:\n%s" % (context_alias, output))

    return output

@task
def deploy_site(site_alias, backup, old_uri=None):
    """Deploy a site stored in the given Aegir backup.

    Note that this taks will not create a new site on the frontend. Additional
    tasks are required to import and enable the site.
    """

    opts = [site_alias, 'provision-deploy', backup]

    if old_uri:
        opts.append('--old_uri')
        opts.append(old_uri)

    output = drush.run(*opts)

    if output.failed:
        error("Deploy of site %s failed:\n%s" % (site_alias, output))

    return output

@task
def enable(context_alias):
    """Enable the given context."""

    output = drush.run(context_alias, 'provision-enable')

    if output.failed:
        error("Enable of %s failed:\n%s" % (context_alias, output))

    return output

@task
def install_site(site_alias, profile):
    """Install a site with the given install profile.

    This creates the site directory and database on the appropriate platform.
    """

    output = drush.run('provision-install', site_alias, '--profile=' + profile)

    if output.failed:
        error("Install of %s failed:\n%s" % (site_alias, output))

    return output

@task
def migrate_site(site_alias, platform_alias, new_site_alias=None):
    """Move or rename a site.

    Depending on the values of the parameters, this task will either rename a
    site on the current platform, move it to a new platform under the same name,
    or both move and rename the site.

    Note that the migration will not be detected on the frontend; this will
    require using the "hosting_import_site" task to allow Aegir to detect the
    change.

    In addition, a rename operation will not delete the site node for the old
    name, requiring manual intervention to clean up old notes. It is recommended
    not to import sites into the fontend if they will be renamed, in order to
    avoid this situation.
    """

    opts = [site_alias, 'provision-migrate', platform_alias]

    if new_site_alias:
        opts.append(new_site_alias)

    output = drush.run(*opts)

    if output.failed:
        error("Migration of %s failed:\n%s" % (site_alias, output))

    return output

@task
def save_platform(platform_alias, **kwargs):
    """Save a new platform alias.

    Note that this will not register as a task on the Aegir frontend.
    """

    kwargs['context_type'] = 'platform'
    kwargs['web_server'] = alias.server(kwargs['web_server'])

    return save(platform_alias, **kwargs)

@task
def save_site(site_alias, **kwargs):
    """Save a new site alias.

    Note that this will not register as a task on the Aegir frontend.
    """

    kwargs['context_type'] = 'site'
    kwargs['db_server'] = alias.server(kwargs['db_server'])
    kwargs['platform'] = alias.platform(kwargs['platform'])

    return save(site_alias, **kwargs)

@task
def verify(context_alias):
    """Verify the given context."""

    output = drush.run(context_alias, 'provision-verify')

    if output.failed:
        error("Verify of %s failed:\n%s" % (context_alias, output))

    return output
