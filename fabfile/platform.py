#!/usr/bin/env python
"""Defined tasks that pertain to platform contexts"""

import os

import fabfile.alias as alias
import fabfile.config as config
import fabfile.drush as drush

from fabric.api import abort, execute, put, sudo, task

@task
def build(platform_name, profile_path):
    """Build a platform on the remote server with the given profile."""

    host = config.get('host')
    profile_path = os.path.realpath(profile_path)
    profile_name = os.path.basename(profile_path)
    temp_dir = host.get('temp_dir', '/tmp')

    result = put(profile_path, temp_dir)

    if result.failed:
        abort("Platform deployment failed.")

    remote_profile_path = temp_dir + '/' + profile_name
    makefile_path = "%s/%s.make" % (remote_profile_path, profile_name)
    platform_path = "%s/platforms/%s" % (host['aegir_root'], platform_name)

    drush.run('make', makefile_path, platform_path)
    sudo("cp --recursive %s %s/profiles" % (remote_profile_path, platform_path))

@task
def save(platform_name):
    """Load the configuration for a platform and provision-save it.

    Note that this task will not create a node on the frontend. Run
    "hosting_import_platform" or "deploy" to build a new platform from this
    task.
    """

    host = config.get('host')
    platform = config.get('platforms').get(platform_name)

    if platform is None:
        abort("Platform %s is not defined." % platform_name)

    root = "%s/platforms/%s" % (host['aegir_root'], platform_name)

    execute('provision.save_platform', platform_name, **{
        'web_server': platform['web_server'],
        'root': root
    })

@task
def verify_all():
    """Verify all platforms."""

    platforms = alias.get_all_records(context_type='platform')

    for platform_alias in platforms:
        execute('provision.verify', platform_alias)
