#!/usr/bin/env python
"""Execution of library tasks via Jenkins"""

import fabfile.config as config

from jenkins import JenkinsException
from fabric.api import task
from fabric.utils import error

@task
def build_job(server, job, **kwargs):
    """Build a job on a continuous integration server."""

    server = config.get('build_servers').get(server)
    instance = server['instance']
    jobs = server['jobs']

    try:
        instance.build_job(jobs[job], kwargs)
    except JenkinsException as exception:
        error('Job %s failed due to: %s' % (jobs[job], str(exception)))
