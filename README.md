# Aegir Fabric Library

A small library of tasks and helper functions to streamline tasks with the Aegir
3 hosting system.

## Requirements

- Python 2.7
- pip

## Dependencies

See the contents of ``requirements.txt``.

## Installation

Install pip using the method of choice for your system. pip is a package manager
for Python, and is required in order to provision the dependencies of this
  library.

Before running any tasks for the first time, use ``pip`` to install all
dependencies.

```sh
$ pip --verbose install --user --requirement requirements.txt
```

## Usage

Fabric tasks are run via the commandline ``fab`` executable. To run a task, type
the ``fab`` command, follow with a space, an then the full name of the task.

Note that angle brackets are used to indicate placeholders in the following
examples; these do not appear in the literal command.

```sh
$ fab <task>
```

To list the available Fabric tasks, use the command:

```sh
$ fab --list
```

This will print out a list of the tasks that you can invoke, along with a brief
description explaining what the task does. Many tasks have a more detailed
description located in the source code file for that task.

Running multiple tasks per fab invokation is possible by simply listing them in
order, separated by spaces.

```sh
$ fab <task1> <task2> ... <taskn>
```

Tasks will run sequentially in the order that they are listed in the command
invokation. This means that if task A requires something from task B, task B
must be entered first in the command, before task A.

Arguments are passed to a fab task as a comma-separated list after a colon:

```sh
$ fab <task>:<arg1>,<arg2>,...,<argn>
```

The colon is not used if no arguments are passed to the task.

Keyword arguments are passed as normal in Python:

```sh
$ fab <task>:arg1=val1,arg2=val2,...,argn=valn
```

All tasks in this library require that the ``config.load`` task be run first.
This task must always run once and only once per invokation of the fab command,
and before any other task.

```sh
$ fab config.load <submodule>.<task>:[<arguments...>]
```

## Structure

By default, Fabric looks for a "fabfile" in the current working directory. This
is either a single file named ``fabfile.py`` or a Python module named
``fabfile``. This library uses the latter option to facilitate neater code
organization.

#### config

Tasks related to loading and updating the YAML configuration files. The
``config.load`` task is required by all tasks in this library, in order for
Fabric to know about the remote host against which all tasks will be run.

#### hosting

Tasks related the the Aegir Hosting component; the front-end. These tasks will
return immediately after hand-off to the front-end. As a result, they
automatically call ``drush @hostmaster hosting-tasks`` and wait for a minimum of
30 seconds. This is to reduce the likelyhood of concurrently running conflicting
Hosting tasks. Even so, care should be taken to avoid sending Aegir into an
invalid state; neither the front-end nor the hosting Drush component will do
anything to prevent such a situation.

#### provision

Tasks related the the Aegir Provision component; the back-end tasks that power
the front-end. These will block until the executed task has completed. The
results of these tasks will not be reflected on the front-end. To update the
front-end, some combination of hosting ``verify`` or ``import`` tasks must be
run.

#### aegir

Helper functions for working with Aegir, such as generating machine names for
the various context types (currently server, platform, and site).

#### drush

Drush library functions. This includes a helper function for running drush
commands locally or on the remote server, as well as loading one or all drush
aliases into a Python dictionary.

#### job

Complex tasks that rely on one or more tasks from the core libraries -
provision, hosting, drush, etc. This is also where tasks that require iterating
over configuration should go, such as bulk migrations.

### Configuration

Configuration is stored in YAML files for easy reading and editing. The settings
stored in these files cover the servers, platforms, and sites in Aegir, as well
as the configuration needed to define and connect to the remote host where Aegir
is running.

#### local

Configuration for the local environment.

- User account to make SSH connections
- SSH agent forwarding

#### host

Configuration for the remote host.

- URI of the host
- Aegir user (used as sudo user for fabric ``sudo()``)
- Aegir root directory
- Path to temporary files directory (e.g. /tmp)

#### platforms

Configuration for the platforms available in Aegir.

- Web server where the platform is installed
- Install profile used to build the platform

#### sites

Configuration for sites that are installed or will be installed on Aegir.

- Current platform where the site is located
- Database server to which the site is connected

#### upgrade

Defines the upgrade mapping for each defined platform. All sites on platform X
will therefore be cloned or migrated to the platform Y defined in this
configuration file.

## Development

### Python

[Python Documentation](https://docs.python.org/2)

Some familiarity with Python, basic scripting, and software development patterns
are required to work effectively with this codebase.

Please observe best practices when coding. Use an editor that automatically
formats your code according to the PEP8 style guide. Avoid leaving untested
code at the head of the master branch. Use Pylint to detect basic errors early
and keep code streamlined.

### Fabric

[Fabric Documentation](http://docs.fabfile.org/en)

Fabric tasks are defined like normal Python functions, with the addition of the
``@task`` decorater. Tasks can contain any valid Python code; they may invoke
other Fabric tasks or Python functions.

Any function defined as a task is then available on the commandline via ``fab``,
as documented above under Usage.

### Organization

The library code is organized to enforce a basic separation of concerns.
Dedicated files consolidate functions related to specific applications or Aegir
components; for example, ``provision.py`` and ``hosting.py`` contain direct
mappings to the Drush commands exposed by these projects. ``drush.py`` and
``aegir.py`` provide low-level functions that are used by higher-level tasks to
interface with the Drush/Aegir back-end. ``job.py`` provides high-level,
multi-step tasks that rely on the mid-level Provision and Hosting tasks.

### Provision and Hosting Tasks

These tasks should be limited to wrappers around defined Aegir - Drush commands;
custom tasks should be defined in ``job.py``. Avoid any access to the global
Fabric env dictionary or the configuration access functions defined by this
library.  Instead, prefer passing all data as parameters, to ensure a clear
interface for each task or function.

Where possible, parameters should correspond to the options and arguments
accepted by the corresponding Drush command. Use logical and predictable naming
conventions that reflect the same language as used in Drush and Aegir. Clearly
document any deviation from the interface for a drush command.

### Job

New "jobs" should be added to ``job.py``. Tasks appropriate for this file
are those that pull some or all of their configuration directly from the
configuration files, such as a bulk migrate task that must iterate over each
site to perform additional operations.

Tasks should be invoked using the Fabric API call ``execute()``, passing the
task name as a string literal. Avoid invoking tasks directly as a plain function
call, as this causes the code to execute outside the Fabric execution context.

Avoid invoking ``drush()`` in ``jobs.py``. Use an appropriate ``provision.*`` or
``hosting.*`` task instead.

Clearly document each job; PEP8 requires a brief, one-line summary at the start
of the doc block, followed by a more complete and detailed explanation. In
particular, note any workarounds or non-standard procedures, edge cases, and
unexpected behavior.
